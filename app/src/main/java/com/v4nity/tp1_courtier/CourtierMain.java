package com.v4nity.tp1_courtier;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

public class CourtierMain extends AppCompatActivity implements View.OnClickListener {
    //Variables globales
    private final int REQUEST_CODE = 1234;
    private TextView tvUsername;
    private ImageButton imButtonSentimentMauvais;
    private ImageButton imButtonSentimentMoyen;
    private ImageButton imButtonSentimentBon;
    private ImageView ivProp;
    private Maison[] proprietes = new Maison[3];
    private Maison propActive;
    private int indexPropActive;
    private String utilisateur;

    //Pour la sauvegarde
    SharedPreferences prefs;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Afin de tester sur mon propre téléphone :)
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        setContentView(R.layout.activity_courtier);

        // Instantiation des widgets
        // Variables locales
        ImageButton imButtonSettings = findViewById(R.id.imButton_settings);
        ImageButton imButtonNext = findViewById(R.id.imButton_next);
        ImageButton imButtonBack = findViewById(R.id.imButton_back);
        ImageButton imButtonMaps = findViewById(R.id.imButton_maps);
        ImageButton imButtonWebsite = findViewById(R.id.imButton_website);
        ImageButton imButtonTelephone = findViewById(R.id.imButton_telephone);

        tvUsername = findViewById(R.id.tvUsername);
        imButtonSentimentMauvais = findViewById(R.id.imButton_mauvais);
        imButtonSentimentMoyen = findViewById(R.id.imButton_moyen);
        imButtonSentimentBon = findViewById(R.id.imButton_bon);
        ivProp = findViewById(R.id.ivProp);

        // Passage de gestion de clics aux boutons
        imButtonSettings.setOnClickListener(this);
        imButtonNext.setOnClickListener(this);
        imButtonBack.setOnClickListener(this);
        imButtonSentimentMauvais.setOnClickListener(this);
        imButtonSentimentMoyen.setOnClickListener(this);
        imButtonSentimentBon.setOnClickListener(this);
        imButtonMaps.setOnClickListener(this);
        imButtonWebsite.setOnClickListener(this);
        imButtonTelephone.setOnClickListener(this);

        // Pour le username fourni dans la page de connexion
        Intent intentUsername = getIntent();
        utilisateur = intentUsername.getStringExtra("username");

        // Création de 3 propriétés par default
        proprietes[0] = new Maison(R.drawable.prop1, -2, "4181111111", "https://fr.airbnb.ca/rooms/34602890?guests=1&adults=1", "29.204468,-94.944453");
        proprietes[1] = new Maison(R.drawable.prop2, -2, "4182222222", "https://architizer.com/projects/earth-house-estate-laettenstrasse/", "47.403877, 8.384367");
        proprietes[2] = new Maison(R.drawable.prop3, -2, "4183333333", "https://guidetoiceland.is/", "65.538616,-19.471278");

        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        for(int i = 0; i < 3 ; i++) {
            int sentiment = prefs.getInt("sentiment_propriete"+(i+1), -2);
            proprietes[i].setSentiment(sentiment);
        }
        // Assigner le nom de l'utilisateur au tvUtilisateur
        tvUsername.setText(getString(R.string.tvUsername)+" "+prefs.getString("username", utilisateur));
        // Afficher la première propriété
        propActive = proprietes[0];
        UpdateUI();
    }

    @Override
    public void onClick(View v) {
        // Déclaration d'Intent pour la navigation
        Intent intent;

        // Switch sur l'Id du bouton
        switch (v.getId()) {
            // Ouverture de l'activité de configuration (les settings)
            case R.id.imButton_settings:
                intent = new Intent(CourtierMain.this, CourtierSettings.class);
                intent.putExtra("username", utilisateur);
                startActivityForResult(intent, REQUEST_CODE);
                break;
            // Trouver la propriété suivante en utilisant la méthode modulo
            case R.id.imButton_next:
                indexPropActive = (indexPropActive + proprietes.length + 1) % proprietes.length;
                propActive = proprietes[indexPropActive];
                UpdateUI();
                break;
            // Trouver la propriété précédente en utilisant la méthode modulo
            case R.id.imButton_back:
                indexPropActive = (indexPropActive + proprietes.length - 1)%proprietes.length;
                propActive = proprietes[indexPropActive];
                UpdateUI();
                break;
            // Ouverture de l'application pour naviguer le web
            case R.id.imButton_website:
                intent = new Intent(Intent.ACTION_VIEW, Uri.parse(propActive.getSiteWeb()));
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivity(Intent.createChooser(intent, "Choose Application"));
                }
                break;
            // Ouverture de l'application téléphone
            case R.id.imButton_telephone:
                intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:"+propActive.getNumTelephone()));
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivity(intent);
                }
                break;
            // Ouverture de l'application maps
            case R.id.imButton_maps:
                intent = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:"+propActive.getAdresse()));
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivity(intent);
                }
                break;
            // Gestion des clicks pour les différents sentiments
            case R.id.imButton_mauvais:
                propActive.setSentiment(-1);
                UpdateUI();
                break;
            case R.id.imButton_moyen:
                propActive.setSentiment(0);
                UpdateUI();
                break;
            case R.id.imButton_bon:
                propActive.setSentiment(1);
                UpdateUI();
                break;
            // FIN Gestion des clicks pour les différents sentiments
            default:
                break;
        }
    }

    // Cette fonction permet d'actualiser les sentiments ainsi que l'image de la propriété(maison) active
    private void UpdateUI(){
        // Afficher la bonne image
        ivProp.setImageResource(propActive.getImgMaison());

        //Enlever la couleur des sentiments et réduire l'opacité des image buttons
        imButtonSentimentMauvais.setColorFilter(Color.argb(100,0,0,0));
        imButtonSentimentMoyen.setColorFilter(Color.argb(100,0,0,0));
        imButtonSentimentBon.setColorFilter(Color.argb(100,0,0,0));

        //Dépendament du sentiment actif, on change l'opacité du image button (J'ai aussi changé la couleur, c'est un extra :) )
        switch (this.proprietes[this.indexPropActive].getSentiment()){
            case -1:
                imButtonSentimentMauvais.setColorFilter(Color.argb(255,210,30,30));
                imButtonSentimentMauvais.setImageAlpha(255);
                imButtonSentimentMoyen.setImageAlpha(100);
                imButtonSentimentBon.setImageAlpha(100);
                break;
            case 0:
                imButtonSentimentMoyen.setColorFilter(Color.argb(255,255,170,30));
                imButtonSentimentMauvais.setImageAlpha(100);
                imButtonSentimentMoyen.setImageAlpha(255);
                imButtonSentimentBon.setImageAlpha(100);
                break;
            case 1:
                imButtonSentimentBon.setColorFilter(Color.argb(255,50,170,50));
                imButtonSentimentMauvais.setImageAlpha(100);
                imButtonSentimentMoyen.setImageAlpha(100);
                imButtonSentimentBon.setImageAlpha(255);
                break;
            default:
                imButtonSentimentMauvais.setImageAlpha(100);
                imButtonSentimentMoyen.setImageAlpha(100);
                imButtonSentimentBon.setImageAlpha(100);
                break;

        }

    }

    // Il est recommandé par Android de faire la sauvegarde des données que nous devons récupérer
    // après la fermeture de l'application lors du onPause(). J'aurai pu utiliser le Bundle, mais
    // puisque les informations qui pourraient être stockés dans le Bundle sont identiques à ceux que
    // je dois récupérer après la fermeture, je peux aussi bien le faire dans le sharedPreferences
    // source: https://developer.android.com/codelabs/android-training-shared-preferences#3

    // De plus, j'utiliserai le getDefaultSharedPreferences() afin de ne pas être obligé de renommer
    // mon fichier de sauvegarde. onPause() va aussi gérer la rotation de l'écran.

    @Override
    protected void onPause() {
        super.onPause();
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = prefs.edit();
        // Sauvegarde des sentiments
        editor.putInt("sentiment_propriete1",proprietes[0].getSentiment());
        editor.putInt("sentiment_propriete2",proprietes[1].getSentiment());
        editor.putInt("sentiment_propriete3",proprietes[2].getSentiment());
        // Sauvegarde du nom d'utilisateur
        editor.putString("username", utilisateur);
        editor.apply();
    }

    // Fonction de rappel pour SousActivity.class
    @SuppressLint("SetTextI18n")
    protected void onActivityResult(int req, int res, Intent data) {
        super.onActivityResult(req, res, data);
        if (req == REQUEST_CODE) {
            if (res == RESULT_OK) {
                //Future-proof pour différentes commandes :D
                if(data.getStringExtra(CourtierSettings.COMMAND) != null) {
                    String command = data.getStringExtra(CourtierSettings.COMMAND);
                    //Pour la réinitialisation des sentiments
                    if (command.equals("reset_sentiments")) {
                        for (Maison propriete : proprietes) {
                            propriete.setSentiment(-2);
                        }
                        UpdateUI();
                    }
                    //Pour le changement du nom d'utilisateur
                    if (command.equals("reset_username")) {
                        String username_reset = data.getStringExtra(CourtierSettings.DATA);
                        tvUsername.setText(getString(R.string.tvUsername) + " " + username_reset);
                        utilisateur = username_reset;
                    }
                    //...if (command.equals("reset_..."))
                    //...
                }
            } else {
                Toast.makeText(getApplicationContext(), R.string.erreur, Toast.LENGTH_LONG).show();
            }
        }
    }
}