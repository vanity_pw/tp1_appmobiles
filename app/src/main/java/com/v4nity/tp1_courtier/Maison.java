package com.v4nity.tp1_courtier;

public class Maison {
    private final int imgMaison;
    private int sentiment;
    private final String numeroTel;
    private final String siteweb;
    private final String adresse;

    /**
     * @param imgProp L'image de la propriété
     * @param sentiment Le sentiment attribué à cette propriété
     *                  Types de sentiments:
     *                     -2 : Valeur par défault, aucun sentiment
     *                     -1 : L'utilosateur donne un mauvais sentiment
     *                      0 : L'utilisateur donne un sentiment neutre
     *                      1 : L'utilisateur donne un bon sentiment
     *
     * @param numeroTel Le numéro de téléphone du propriétaire ou de l'agent associé à cette propriété
     * @param siteweb Le site-web de la propriété ou de l'annonce
     * @param adresse L'addresse de la propriété
     */
    public Maison(int imgProp, int sentiment, String numeroTel, String siteweb, String adresse) {
        this.imgMaison = imgProp;
        this.sentiment = sentiment;
        this.numeroTel = numeroTel;
        this.siteweb = siteweb;
        this.adresse = adresse;
    }

    //Début getters
    public int getImgMaison() {
        return imgMaison;
    }

    public int getSentiment() {
        return sentiment;
    }

    public String getNumTelephone() {
        return numeroTel;
    }

    public String getSiteWeb() {
        return siteweb;
    }

    public String getAdresse() {
        return adresse;
    }
    //Fin getters

    //Début setters
    public void setSentiment(int sentiment) {
        this.sentiment = sentiment;
    }
    //Fin setters

}
