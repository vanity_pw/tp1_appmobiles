package com.v4nity.tp1_courtier;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;


public class CourtierSettings extends AppCompatActivity {
    //Variables globales
    public static final String MESSAGE = "MESSAGE";
    public static final String COMMAND = "COMMAND";
    public static final String DATA = "DATA";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Afin de tester sur mon propre téléphone :)
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        setContentView(R.layout.activity_settings);

        //Variables locales
        Intent intentUsername = getIntent();
        String utilisateur = intentUsername.getStringExtra("username");
        Button btResetUsername = findViewById(R.id.btResetUsername);
        Button btResetEmoticons = findViewById(R.id.btResetEmoticons);
        EditText etUsernameReset = findViewById(R.id.etUsernameReset);


        etUsernameReset.setText(utilisateur);
        //Changement du nom d'utilisateur
        btResetUsername.setOnClickListener(v -> {
            String command = "reset_username";
            String username = etUsernameReset.getText().toString();
            // Création d'une intention pour retour
            Intent intent = new Intent();
            intent.putExtra(COMMAND, command);
            intent.putExtra(DATA, username);
            setResult(RESULT_OK, intent);
            finish();

        });
        //Réinitialisation des sentiments / emoticons
        btResetEmoticons.setOnClickListener(v -> {
            String command = "reset_sentiments";
            // Création d'une intention pour retour
            Intent intent = new Intent();
            intent.putExtra(COMMAND, command);
            setResult(RESULT_OK, intent);
            finish();

        });
    }
}

