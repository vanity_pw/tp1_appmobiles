package com.v4nity.tp1_courtier;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText etUsername, etPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Afin de tester sur mon propre téléphone :)
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

        setContentView(R.layout.activity_main);
        Button btLogin = findViewById(R.id.btLogin);
        etUsername = findViewById(R.id.etUsername);
        etPassword = findViewById(R.id.etPassword);
        btLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(etUsername.getText().toString().equals("bot") && etPassword.getText().toString().equals("123")) {
                    String username = etUsername.getText().toString();
                    Intent intent = new Intent(MainActivity.this, CourtierMain.class);
                    //Pour que l'autre activité puisse récupérer le nom de l'utilisateur
                    intent.putExtra("username", etUsername.getText().toString());
                    startActivity(intent);
                }else{
                    Toast.makeText(getApplicationContext(), R.string.erreur_login, Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}